US Diversity, Integration, and Segregation
==========================================

This project contains the source code for a 
video series that explains the concepts of 
diversity, integration, and segregation and
illustrates them with examples from various
communities in the United States.

These videos were built using the 
[gewel](https://gewel.readthedocs.io/) animation
system.

![Sample 1](docs/sample1.gif)
