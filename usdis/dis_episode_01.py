import argparse
import cProfile
import dataclasses
import io
import itertools
import pstats
import random
import sys
from enum import IntEnum
from time import perf_counter
from typing import Callable, Iterable, List, Optional, Tuple

import gewel
import gewel.color
import gewel.draw
import gewel.player
import gewel.record
import tvx
import tvx.utils


@dataclasses.dataclass()
class DotMapper:
    """
    This is alpha class that manages the positions of dots that
    represent people in alpha rectangle than represents alpha
    community. It is organized as an outer rectangle containing
    smaller inner rectangles that have rows and columns of
    dots. Spaces are left between the inner rectangles when
    the dot positions for plotting are computed.

    This class has an inner width and height that indicate
    how many people are in alpha small rectangle and an outer width
    and height that indicate how many small rectangles there
    are across and down.
    """
    w_outer: int = 3
    h_outer: int = 6
    w_inner: int = 10
    h_inner: int = 4
    spacing: int = 48
    x_offset: float = 0.0
    y_offset: float = 0.0

    def pos(self, x_outer: int, y_outer: int, x_inner: int, y_inner: int) -> Tuple[float, float]:
        """
        Compute an x, y position for plotting the dot at the given
        four-dimensional point.

        Parameters
        ----------
        x_outer: The horizontal index of the rectangle the dot belongs to.
        y_outer: The vertical index of the rectangle the dot belongs to.
        x_inner: The horizontal index of the dot within the rectangle.
        y_inner: The vertical index of the dot within the rectangle.

        Returns
        -------
        x, y coordinates for plotting the dot.
        """
        x = x_outer * (self.spacing * (1 + self.w_inner)) + (1 + x_inner) * self.spacing + self.x_offset
        y = (self.h_outer - 1 - y_outer) * (self.spacing * (1 + self.h_inner)) + \
            (self.h_inner - y_inner) * self.spacing + self.y_offset

        return x, y

    @property
    def center(self) -> Tuple[float, float]:
        x = 0.5 * (self.w_outer * (self.w_inner + 1) + 1) * self.spacing
        y = 0.5 * (self.h_outer * (self.h_inner + 1) + 1) * self.spacing

        return x, y

    def __len__(self):
        return self.h_outer * self.w_outer * self.w_inner * self.h_inner

    @property
    def dim(self) -> Tuple[int, int]:

        x = (self.w_outer * (self.w_inner + 1)) * self.spacing
        y = (self.h_outer * (self.h_inner + 1)) * self.spacing

        return x, y

    def positions(self) -> List[Tuple[int, int, int, int]]:
        """
        Compute the positions of all dots.
        """
        return [
            (ii, jj, x, y)
            for ii in range(self.w_outer)
            for jj in range(self.h_outer)
            for y in range(self.h_inner)
            for x in range(self.w_inner)
        ]

    def random_positions(self) -> List[Tuple[int, int, int, int]]:
        """
        Return positions of all the dots in random order. This
        is useful for computing target positions for them all to
        move to if we want to randomly shuffle them on the screen,
        e.green. to demonstrate integration.
        """
        positions = self.positions()
        random.shuffle(positions)
        return positions

    def neighborhoods(self) -> List[List[Tuple[int, int, int, int]]]:
        """
        For the purposes of this video, we divide the rectangle into
        three neighborhoods. This method returns alpha list with one element
        per neighborhood. Each element is itself alpha list of tuples of the
        form (x_outer, y_outer, width, height) representing rectangles
        of dots at the outer level that are in the neighborhood.
        """
        return [
            [
                (0, 0, 2 * self.w_outer // 3, self.h_outer // 3),
                (0, self.h_outer // 3, self.w_outer // 3, 2 * self.h_outer // 3),
            ],
            [
                (2 * self.w_outer // 3, 0, self.w_outer, self.h_outer // 3),
                (self.w_outer // 3, self.h_outer // 3, self.w_outer, 2 * self.h_outer // 3),
            ],
            [
                (0, 2 * self.h_outer // 3, self.w_outer, self.h_outer)
            ],
        ]

    def neighborhood_positions(self) -> List[List[Tuple[int, int, int, int]]]:
        """
        Compute the positions in (x_outer, y_outer, x_inner, y_inner) space
        for all of the points in each of the neighborhoods.
        """
        return [
            [
                (ii, jj, x, y)
                for block_spec in neighborhood
                for ii in range(block_spec[0], block_spec[2])
                for jj in range(block_spec[1], block_spec[3])
                for y in range(self.h_inner)
                for x in range(self.w_inner)
            ]
            for neighborhood in self.neighborhoods()
        ]

    # These methods construct PyTvf expressions that indicate whether
    # alpha given point is in the box or in one of its neighborhoods.
    # These expressions are the bottom layer of the PyTvf trees that
    # compute diversity. Doing it this way lets us compute diversity
    # in real time. It can be slow, however, so we tend to try to
    # only sample it at key times, like right before we move alpha bunch
    # of people or right after they land in their new positions.

    def in_box(self, x: tvx.FloatOrTVF, y: tvx.FloatOrTVF) -> tvx.BoolOrTVB:
        x0, y0 = self.x_offset, self.y_offset
        width, height = self.dim
        x1, y1 = x0 + width, y0 + height

        return (x0 < x) & (x < x1) & (y0 < y) & (y < y1)

    def in_neighborhood_a(self, x_: tvx.FloatOrTVF, y_: tvx.FloatOrTVF) -> tvx.BoolOrTVB:
        width, height = self.dim

        return ((self.x_offset < x_) & (x_ < self.x_offset + width) &
                (self.y_offset < y_) & (y_ < self.y_offset + height // 3))

    def in_neighborhood_b(self, x_: tvx.FloatOrTVF, y_: tvx.FloatOrTVF) -> tvx.BoolOrTVB:
        width, height = self.dim

        return ((self.x_offset < x_) &
                ((x_ < self.x_offset + 2 * width // 3) &
                 (self.y_offset + 2 * height // 3 < y_) &
                 (y_ < self.y_offset + height)) |
                ((x_ < self.x_offset + width // 3) &
                 (self.y_offset + height // 3 < y_) &
                 (y_ < self.y_offset + 2 * height // 3)))

    def in_neighborhood_c(self, x_: tvx.FloatOrTVF, y_: tvx.FloatOrTVF) -> tvx.BoolOrTVB:
        width, height = self.dim

        return ((self.x_offset + width // 3 < x_) &
                ((x_ < self.x_offset + width) &
                 (self.y_offset + height // 3 < y_) &
                 (y_ < self.y_offset + 2 * height // 3)) |
                ((self.x_offset + 2 * width // 3 < x_) &
                 (self.y_offset + 2 * height // 3 < y_) &
                 (y_ < self.y_offset + height)))


def build_scene(
        scene_width: float, scene_height: float,
        verbose: bool = False, show_clock=False,
        with_teleprompter: bool = False

) -> Tuple[gewel.draw.Drawable, float]:

    random.seed(17194587)

    if verbose:
        print("Building Scene.")

    # Create alpha dot mapper which we rely on to tell us where
    # to position dots and what positions to move them to.
    dm = DotMapper()
    dm.y_offset = (scene_height - dm.dim[1]) / 2
    dm.x_offset = 0.75 * dm.y_offset

    width, height = dm.dim

    # Build alpha box around the part of scene that we will plot
    # the community in. We also use this as alpha sync object
    # all through the scene.

    box_line_width = 12.0

    box = gewel.draw.Box(
        dm.x_offset, dm.y_offset, width, height,
        color=gewel.color.DARK_GRAY, line_width=box_line_width,
        z=0.5
    )

    # Position the teleprompter at the bottom.
    teleprompter = gewel.draw.Teleprompter(
        0, scene_height, scene_width, scene_height / 3,
        font_size=0.1 * scene_height,
        color=gewel.color.YELLOW, fill_color=gewel.color.BLACK,
        lines_per_second=0.6,
        line_width=0.0,
        z=1000.0
    )

    # Construct boxes that will grow and shrink and change color
    # as diversity overall, integration, and diversity within
    # given neighborhoods change. We will call the neighborhoods
    # alpha, blue, and c.

    # Indices into our array of bars.

    class BarId(IntEnum):
        BAR_DIVERSITY_IN_ALL = 0
        BAR_INTEGRATION = 1
        BAR_DIVERSITY_IN_A = 2
        BAR_DIVERSITY_IN_B = 3
        BAR_DIVERSITY_IN_C = 4

        NUM_DIS_BARS = 5

    # Some dimensions for the bars.

    div_box_width = 0.06 * scene_width

    div_label_height = 180
    max_div_box_height = box.height - div_label_height

    div_box_x = scene_width * 0.75 - div_box_width / 2 - dm.x_offset / 3
    div_box_y = box.y + box.height - div_label_height

    div_bar_line_width = 5

    # We will put some labels under the bars also.

    div_label_center_x = div_box_x + 0.5 * div_box_width
    div_label_y = div_box_y + 30

    # Each bar has its own color map. We will dynamically adjust
    # the position of the color maps so that bars change color
    # as diversity changes.
    bar_colors = [gewel.color.fire() for _ in range(BarId.NUM_DIS_BARS)]

    fire_color_map_box = bar_colors[BarId.BAR_DIVERSITY_IN_ALL]
    fire_color_map_int = bar_colors[BarId.BAR_INTEGRATION]
    fire_color_map_a = bar_colors[BarId.BAR_DIVERSITY_IN_A]
    fire_color_map_b = bar_colors[BarId.BAR_DIVERSITY_IN_B]
    fire_color_map_c = bar_colors[BarId.BAR_DIVERSITY_IN_C]

    # The bars themselves.

    dis_bars = [
        gewel.draw.Box(
            div_box_x + (1.5 * (ii - 2) + 0.25) * div_box_width, div_box_y,
            width=div_box_width, height=0.0,
            color=bar_colors[ii], fill_color=bar_colors[ii],
            line_width=div_bar_line_width,
            z=0.5
        )
        for ii in range(BarId.NUM_DIS_BARS)
    ]

    # Special case settings.
    dis_bars[BarId.BAR_DIVERSITY_IN_ALL].x = div_box_x

    segregation_bar_color = gewel.color.Color(0.85, 0.85, 0.85)

    segregation_bar = gewel.draw.Box(
        dis_bars[BarId.BAR_INTEGRATION].x,
        dis_bars[BarId.BAR_INTEGRATION].y + dis_bars[BarId.BAR_INTEGRATION].height,
        dis_bars[BarId.BAR_INTEGRATION].width, height=0.0,
        color=segregation_bar_color, fill_color=segregation_bar_color,
        line_width=div_bar_line_width,
        z=0.25
    )

    # A diversity label used initially when there is only one
    # bar shown.

    diversity_label = gewel.draw.TextBox(
        'Diversity',
        div_label_center_x - 2 * div_box_width, div_label_y,
        4 * div_box_width, div_label_height,
        font_size=100, color=gewel.color.DARK_GRAY, justification=gewel.draw.TextJustification.CENTER)

    box.wait(1.0)

    teleprompter.wait_for(box)
    teleprompter.add_text(
        "Hi, my name is Vivienne.\n\n" +
        "I'll be your host today on this video my dad and I made about diversity, integration " +
        "and segregation.\n"
    )

    teleprompter.add_text(
        "In order to talk about these concepts, we are going to use this box to represent a " +
        "community."
    )

    box.wait_for(teleprompter)
    box.wait(2.0)

    key_times = [0.0]

    marker_width = dm.spacing * 0.8

    x_left = -scene_height
    y_left = scene_height / 2

    x_top = scene_height / 2
    y_top = -scene_height

    x_corner_off_stage = x_left
    y_corner_off_stage = y_top

    n_by_3 = len(dm) // 3

    category10_indices = [0, 2, 4]

    drawables = [
        [
            gewel.draw.MarkerDot(
                x_left, y_left,
                width=marker_width, line_width=4,
                color=gewel.color.category10(category10_indices[ii]), z=1.0
            )
            for _ in range(n_by_3)
        ]
        for ii in range(3)
    ]

    scene = gewel.draw.Scene(
        itertools.chain(*[[d for d in drawables_ii] for drawables_ii in drawables])
    )

    scene.add(gewel.draw.Background())

    if with_teleprompter:
        scene.add(teleprompter)

    scene.add(box)

    for bar in dis_bars:
        scene.add(bar)

    scene.add(segregation_bar)

    scene.add(diversity_label)

    if show_clock:
        clock = gewel.draw.TimeClock(100, scene_height - 100, font_size=72, z=100)
        scene.add(clock)

    positions = dm.positions()
    random_positions = dm.random_positions()

    positions = [
        positions[:n_by_3],
        positions[n_by_3: 2 * n_by_3],
        positions[2 * n_by_3: 3 * n_by_3]
    ]

    def move_all_to_positions(
            drawables_: Iterable[gewel.draw.XYDrawable],
            destination_positions: Iterable[Tuple[int, int, int, int]],
            duration: float = 3.0,
            gap: float = 0.01
    ):
        """
        Move a bunch of drawables to destination positions in the
        4D space of dm to the corresponding 2D location in the
        scene.
        """
        key_times.append(box.time)
        a = None
        t_first_entry = None
        for a, p in zip(drawables_, destination_positions):
            a.wait_for(box)
            x0 = a.x
            if isinstance(x0, tvx.Tvf):
                x0 = x0(box.time)
            dmx, dmy = dm.pos(*p)
            if (x0 < box.x) and (dmx >= box.x):
                t_enter = box.time + duration * (box.x - x0) / (dmx - x0)
                t_first_entry = min(t_enter, t_first_entry if t_first_entry is not None else t_enter)
            a.move_to(dmx, dmy, duration=duration)
            box.wait(gap)
        if a is not None:
            if t_first_entry is not None:
                key_times.append(t_first_entry)
            key_times.append(a.time)

    def move_some_to_positions(
            group: int, start: int, end: int,
            duration: float = 3.0,
            destination_positions: Optional[List[List[Tuple[int, int, int, int]]]] = None
    ):
        """
        Move a subset of drawables defined by a range of indices, to
        new positions.
        """
        if destination_positions is None:
            destination_positions = positions
        move_all_to_positions(drawables[group][start:end], destination_positions[group][start:end], duration=duration)

    def exeunt(drawables_: Iterable[gewel.draw.XYDrawable], duration: float = 1.5):
        d_list = list(drawables_)
        d_list.reverse()
        key_times.append(box.time)
        d = None
        for d in d_list:
            d.wait_for(box)
            d.move_to(x_top, y_top, duration=duration)
            d.move_to(x_corner_off_stage, y_corner_off_stage, duration=0.0)
            d.move_to(x_left, y_left, duration=0.0)
            box.wait(0.0025)
        if d is not None:
            key_times.append(d.time)

    def exeunt_some(group: int, start: int, end: int, duration: float = 3.0,):
        exeunt(drawables[group][start:end], duration=duration)

    teleprompter.add_text(
        "Let's start by adding some people to the community.",
        box.time
    )

    box.wait_until(teleprompter.time)

    teleprompter.add_text(
        "Imagine each of these dots is one person."
    )

    # No diversity.
    move_some_to_positions(0, 0, 100)
    box.wait(5)

    gewel.draw.sync([box, teleprompter])

    teleprompter.add_text(
        "We just added a hundred people, but as you can see, they all look the same.\n" +
        "We will use different colors to represent different groups of people. The " +
        "actual colors we use—blue, green, purple, don't stand for anything in " +
        "particular."
    )
    gewel.draw.sync([box, teleprompter])
    teleprompter.add_text(
        "They just mean that people represented by different colors " +
        "are in some sort of different group."
    )

    gewel.draw.sync([box, teleprompter])

    move_some_to_positions(1, 0, 4)
    teleprompter.add_text(
        "And look. Here comes a different group of people now."
    )
    gewel.draw.sync([box, teleprompter])

    box.wait(4.5)

    gewel.draw.sync([box, teleprompter])

    # A little more
    move_some_to_positions(2, 0, 4)
    teleprompter.add_text(
        "And here are some people from a third group.\n\n" +
        "Notice that when we added people from the second and third groups, " +
        "a little bar labeled diversity started to grow.\n\nWe won't get into " +
        "the math of exactly how we compute diversity. But it does look like " +
        "it is moving in the right direction. As new people came in from the second " +
        "and third group, the bar got larger.\n"
    )
    box.wait(4.5)

    gewel.draw.sync([box, teleprompter])
    teleprompter.add_text(
        "This trend will continue if we add some more people to one of the smaller " +
        "groups.\n"
    )

    # A little more
    move_some_to_positions(1, 4, 10)
    box.wait(4.5)

    gewel.draw.sync([box, teleprompter])
    teleprompter.add_text(
        "Now let's add even more to the second group.\n"
    )

    # A little more
    move_some_to_positions(1, 10, 25)
    box.wait(4.5)

    gewel.draw.sync([box, teleprompter])
    teleprompter.add_text(
        "As you can see, as we keep adding more to the second group, diversity keeps going up.\n"
    )

    gewel.draw.sync([box, teleprompter])
    teleprompter.add_text(
        "And if we add more to the third group, which is now by far the smallest, then " +
        "diversity also goes up.\n"
    )

    move_some_to_positions(2, 4, 25)
    box.wait(4.5)

    gewel.draw.sync([box, teleprompter])
    teleprompter.add_text(
        "But this effect only goes so far. As one of the smaller groups starts to " +
        "get close to the size of the largest group, the diversity increases start " +
        "to slow down.\n"
    )

    move_some_to_positions(1, 25, 100)
    box.wait(5)

    gewel.draw.sync([box, teleprompter])
    teleprompter.add_text(
        "Now the second group is just as large as the first group. So what happens " +
        "if we keep increasing it's size?\n"
    )

    move_some_to_positions(1, 100, 180)
    box.wait(5)

    gewel.draw.sync([box, teleprompter])
    teleprompter.add_text(
        "As you can see, when it became the largest group and continued to grow, " +
        "diversity actually started to go down.\n"
    )

    gewel.draw.sync([box, teleprompter])
    teleprompter.add_text(
        "So what are we learning here? The pattern that seems to be emerging is that " +
        "if we increase the size of any group that is not currently the largest, then " +
        "diversity goes up.\n"
    )

    # More of the small group and it goes up.
    move_some_to_positions(2, 25, 100)
    box.wait(5)

    gewel.draw.sync([box, teleprompter])
    teleprompter.add_text(
        "But if we increase the size of the largest group then it goes down.\n"
    )

    move_some_to_positions(1, 180, 240)
    box.wait(5)

    gewel.draw.sync([box, teleprompter])
    teleprompter.add_text(
        "If people move out of the community, the opposite happens.\n"
    )

    gewel.draw.sync([box, teleprompter])
    exeunt_some(1, 108, 240)
    teleprompter.add_text(
        "Diversity goes up when the largest group gets smaller.\n"
    )

    gewel.draw.sync([box, teleprompter])
    exeunt_some(2, 18, 100)
    exeunt_some(0, 18, 100)
    teleprompter.add_text(
        "Diversity goes down when one of the smaller groups gets smaller.\n"
    )

    gewel.draw.sync([box, teleprompter])
    teleprompter.add_text(
        "So let's make the groups all the same size now. This will maximize diversity.\n"
    )

    move_some_to_positions(0, 18, 240)
    move_some_to_positions(1, 108, 240)
    move_some_to_positions(2, 18, 240)
    box.wait(6)

    gewel.draw.sync([box, teleprompter])
    teleprompter.add_text(
        "Now let's think a little bit not just about how many people from each " +
        "group at in the community, but where they live relative to one another.\n"
    )

    # Shuffle
    all_drawables_on_stage = list(itertools.chain(*[drawables_ii for drawables_ii in drawables]))
    shuffled_drawables_on_stage = list(all_drawables_on_stage)
    random.shuffle(shuffled_drawables_on_stage)
    move_all_to_positions(shuffled_drawables_on_stage, random_positions, duration=1, gap=0.0)
    box.wait(4)

    gewel.draw.sync([box, teleprompter])
    teleprompter.add_text(
        "The diversity of the overall community didn't change when all of these " +
        "people moved around. But something is different about the community. " +
        "It is much more integrated."
    )

    gewel.draw.sync([box, teleprompter])
    teleprompter.add_text(
        "Now let's look at a different kind of situation. " +
        "The three groups live in three different neighborhoods, without any integration."
    )

    neighborhood_positions = dm.neighborhood_positions()

    for drawables_ii, positions_ in zip(drawables, neighborhood_positions):
        move_all_to_positions(drawables_ii, positions_, gap=0.0, duration=2.0)

    box.wait(5)

    gewel.draw.sync([box, teleprompter])
    teleprompter.add_text(
        "Now maybe it isn't always quite that bad. If a few people move around " +
        "then this individual neighborhoods are a small amount more integrated. "
    )

    # Shuffle a bit in neighborhoods.

    def random_drawables(k_: int, neighborhood_indices: Optional[Iterable[int]] = None) -> \
            List[gewel.draw.XYDrawable]:

        if neighborhood_indices is None:
            all_drawables = list(itertools.chain(*drawables))
        else:
            neighborhood_indices = set(neighborhood_indices)
            all_drawables = list(itertools.chain(*[
                drawables[ii] for ii in range(len(drawables)) if ii in neighborhood_indices
            ]))
        ra_ = random.sample(all_drawables, k_)

        return ra_

    def shuffle_neighborhoods(k, neighborhood_indices: Optional[Iterable[int]] = None):
        key_times.append(box.time)

        ra = random_drawables(k, neighborhood_indices)
        destinations = [drawable_.current_xy(box.time) for drawable_ in ra]
        random.shuffle(destinations)
        drawable = None
        for drawable, (x, y) in zip(ra, destinations):
            drawable.wait_for(box)
            drawable.move_to(x, y, duration=1.5)

        if drawable is not None:
            key_times.append(drawable.time)

        box.wait(5)
        key_times.append(box.time)

    shuffle_neighborhoods(50)

    gewel.draw.sync([box, teleprompter])
    teleprompter.add_text(
        "But during this shuffling, our single measure of community diversity " +
        "didn't change at all. We're going to have to come up with some better way " +
        "of describing what is going on if we want to discuss how a community can " +
        "be diverse, and either very integrated...\n"
    )

    move_all_to_positions(shuffled_drawables_on_stage, random_positions, duration=1, gap=0.0)
    box.wait(4)

    gewel.draw.sync([box, teleprompter])
    teleprompter.add_text(
        "Or not integrated at all.\n"
    )

    # And back to fully segregated.
    for drawables_ii, positions_ in zip(drawables, neighborhood_positions):
        move_all_to_positions(drawables_ii, positions_, gap=0.0, duration=2.0)

    box.wait(5)

    gewel.draw.sync([box, teleprompter])
    teleprompter.add_text(
        "In order to study integration, we are going to start by marking off " +
        "individual neighborhoods in the community."
    )

    # Make room for other labels.
    div_transition_duration = 1.5

    dis_bars[BarId.BAR_DIVERSITY_IN_ALL].wait_for(box)
    diversity_label.wait_for(box)
    dis_bars[BarId.BAR_DIVERSITY_IN_ALL].move_to(
        div_box_x - 2.75 * div_box_width, div_box_y, duration=div_transition_duration
    )
    diversity_label.fade_to(0.0, 0.5, update_time=False)
    diversity_label.move_to(div_label_center_x, scene_height * 2, duration=1.0)

    # Neighborhoods and their labels fade in.

    n_fade_in_time = box.time
    key_times = key_times + [n_fade_in_time, n_fade_in_time + div_transition_duration]

    sub_label_color = gewel.color.ColorMap.from_colors([gewel.color.TRANSPARENT, gewel.color.DARK_GRAY])
    sub_label_color.position = tvx.ramp(0.0, 1.0, n_fade_in_time, div_transition_duration)

    dis_bars[BarId.BAR_DIVERSITY_IN_A].height = tvx.ramp(
        0.0, dis_bars[BarId.BAR_DIVERSITY_IN_A].height,
        n_fade_in_time, div_transition_duration
    )
    dis_bars[BarId.BAR_DIVERSITY_IN_B].height = tvx.ramp(
        0.0, dis_bars[BarId.BAR_DIVERSITY_IN_B].height,
        n_fade_in_time, div_transition_duration
    )
    dis_bars[BarId.BAR_DIVERSITY_IN_C].height = tvx.ramp(
        0.0, dis_bars[BarId.BAR_DIVERSITY_IN_C].height,
        n_fade_in_time, div_transition_duration
    )

    n_label_offset = 0.75 * div_label_height

    n_label_a = gewel.draw.ToyTextDrawable(
        dm.x_offset + width // 2 + 1.75 * n_label_offset, dm.y_offset - n_label_offset + 40,
        'A', color=sub_label_color, font_size=120,
        z=0.5
    )
    n_call_out_a = gewel.draw.PathDrawable(
        [
            dm.x_offset + width // 2,
            dm.x_offset + width // 2 + 0.5 * n_label_offset,
            dm.x_offset + width // 2 + 1.5 * n_label_offset
        ],
        [
            dm.y_offset,
            dm.y_offset - n_label_offset,
            dm.y_offset - n_label_offset
        ],
        color=sub_label_color,
        line_width=box_line_width,
        z=0.5
    )

    n_label_b = gewel.draw.ToyTextDrawable(
        dm.x_offset + width // 3 + 1.75 * n_label_offset, dm.y_offset + height + n_label_offset + 40,
        'B', color=sub_label_color, font_size=120,
        z=0.5
    )

    n_call_out_b = gewel.draw.PathDrawable(
        [
            dm.x_offset + width // 3,
            dm.x_offset + width // 3 + 0.5 * n_label_offset,
            dm.x_offset + width // 3 + 1.5 * n_label_offset
        ],
        [
            dm.y_offset + height,
            dm.y_offset + height + n_label_offset,
            dm.y_offset + height + n_label_offset,
        ],
        color=sub_label_color,
        line_width=box_line_width,
        z=0.5
    )

    n_label_c = gewel.draw.ToyTextDrawable(
        dm.x_offset + 5 * width // 6 + 1.75 * n_label_offset, dm.y_offset + height + n_label_offset + 40,
        'C', color=sub_label_color, font_size=120,
        z=0.5
    )

    n_call_out_c = gewel.draw.PathDrawable(
        [
            dm.x_offset + 5 * width // 6,
            dm.x_offset + 5 * width // 6 + 0.5 * n_label_offset,
            dm.x_offset + 5 * width // 6 + 1.5 * n_label_offset
        ],
        [
            dm.y_offset + height,
            dm.y_offset + height + n_label_offset,
            dm.y_offset + height + n_label_offset,
            ],
        color=sub_label_color,
        line_width=box_line_width,
        z=0.5
    )

    neighborhood_border_a = gewel.draw.PathDrawable(
        [dm.x_offset, dm.x_offset + width],
        [dm.y_offset + height // 3, dm.y_offset + height // 3],
        color=sub_label_color,
        line_width=box_line_width,
        z=0.5
    )

    neighborhood_border_b = gewel.draw.PathDrawable(
        [
            dm.x_offset + width // 3,
            dm.x_offset + width // 3,
            dm.x_offset + 2 * width // 3,
            dm.x_offset + 2 * width // 3,
        ],
        [
            dm.y_offset + height // 3,
            dm.y_offset + 2 * height // 3,
            dm.y_offset + 2 * height // 3,
            dm.y_offset + height,
        ],
        color=sub_label_color,
        line_width=box_line_width,
        z=0.5
    )

    box.wait_for(diversity_label)
    key_times.append(box.time)

    div_label_font_size = 60

    div_label_all = gewel.draw.TextBox(
        'Diversity Overall',
        div_label_center_x - 3.25 * div_box_width, div_label_y,
        div_box_width, div_label_height,
        font_size=div_label_font_size, color=sub_label_color,
        justification=gewel.draw.TextJustification.CENTER)

    div_label_a = gewel.draw.TextBox(
        'Diversity in A',
        div_label_center_x - 0.25 * div_box_width, div_label_y,
        div_box_width, div_label_height,
        font_size=div_label_font_size, color=sub_label_color,
        justification=gewel.draw.TextJustification.CENTER)

    div_label_b = gewel.draw.TextBox(
        'Diversity in B',
        div_label_center_x + 1.25 * div_box_width, div_label_y,
        div_box_width, div_label_height,
        font_size=div_label_font_size, color=sub_label_color,
        justification=gewel.draw.TextJustification.CENTER)

    div_label_c = gewel.draw.TextBox(
        'Diversity in C',
        div_label_center_x + 2.75 * div_box_width, div_label_y,
        div_box_width, div_label_height,
        font_size=div_label_font_size, color=sub_label_color,
        justification=gewel.draw.TextJustification.CENTER)

    scene.add(div_label_all)
    scene.add(div_label_a)
    scene.add(div_label_b)
    scene.add(div_label_c)
    scene.add(n_label_a)
    scene.add(n_label_b)
    scene.add(n_label_c)
    scene.add(n_call_out_a)
    scene.add(n_call_out_b)
    scene.add(n_call_out_c)

    scene.add(neighborhood_border_a)
    scene.add(neighborhood_border_b)

    box.wait(5)
    key_times.append(box.time)

    gewel.draw.sync([box, teleprompter])
    teleprompter.add_text(
        "Now we can look at the diversity of each neighborhood independently. Right now, " +
        "none of the three neighborhoods has any diversity, even though the community as a " +
        "whole is diverse.\n"
    )

    gewel.draw.sync([box, teleprompter])
    teleprompter.add_text(
        "If people move around a bit, we do get some diversity in each of the neighborhoods."
    )

    shuffle_neighborhoods(100)

    box.wait(5.0)

    gewel.draw.sync([box, teleprompter])
    teleprompter.add_text(
        "And if we move them back, then the diversity disappears.\n"
    )

    for drawables_ii, positions_ in zip(drawables, neighborhood_positions):
        move_all_to_positions(drawables_ii, positions_, gap=0.0, duration=2.0)

    box.wait(5)

    gewel.draw.sync([box, teleprompter])

    teleprompter.add_text(
        "So now let's bring the idea of individual neighborhoods being " +
        "diverse together into the concept of integration. We'll add " +
        "another bar to the chart, right next to overall diversity."
    )
    gewel.draw.sync([box, teleprompter])

    n_integration_fade_in_time = box.time

    integration_label_color = gewel.color.ColorMap.from_colors(
        [gewel.color.TRANSPARENT, gewel.color.DARK_GRAY]
    )
    integration_label_color.position = tvx.ramp(0.0, 1.0, n_integration_fade_in_time, div_transition_duration)

    int_label = gewel.draw.TextBox(
        'Integration',
        div_label_center_x - 1.75 * div_box_width, div_label_y,
        div_box_width, div_label_height,
        font_size=div_label_font_size, color=integration_label_color,
        justification=gewel.draw.TextJustification.CENTER)

    scene.add(int_label)

    teleprompter.add_text(
        "Looking at the integration bar, you'll see that it's a light gray color. " +
        "This is meant not to indicate integration, but the potential for integration.\n" +
        "This potential integration bar is always the same height as the overall diversity bar " +
        "at the far left.\n\n" +
        "What this means is that if there has to be community diversity for there to be "
        "integration. Diversity does not guarantee integration, but lack of diversity does " +
        "guarantee a lack of integration.\n"
    )

    teleprompter.add_text(
        "Of course actual—as opposed to potential—integration is zero at the moment, even though " +
        "diversity is high in the community as a whole, because there is no diversity in any of " +
        "the neighborhoods.\n" +
        "Let's see what happens if we change that."
    )

    box.wait(2.0)

    gewel.draw.sync([box, teleprompter])
    teleprompter.add_text(
        "If people move around as before, our new integration bar measures it. It captures " +
        "the idea that diversity went up in each of the three individual neighborhoods by " +
        "combining those effects into a single number."
    )

    shuffle_neighborhoods(100)

    teleprompter.add_text(
        "As people move around. Diversity in the neighborhoods goes up, and actual, as opposed " +
        "to potential, integration goes up too."
    )

    gewel.draw.sync([box, teleprompter])
    teleprompter.add_text(
        "Sometimes we might have diversity in some neighborhoods and not so much " +
        "in others. That help with integration relative to where we just were, but it does " +
        "not get us all the way there."
    )
    shuffle_neighborhoods(480, [1, 2])

    box.wait(2)

    gewel.draw.sync([box, teleprompter])
    teleprompter.add_text(
        "Neighborhoods A and C are now more diverse than B is. That drives integration up, but with " +
        "the diversity of neighborhood B remaining low, there is only so integrated the community can be."
    )
    gewel.draw.sync([box, teleprompter])

    box.wait(2)

    gewel.draw.sync([box, teleprompter])
    teleprompter.add_text(
        "If everyone moves around and all neighborhoods are diverse, then the community can reach its maximum " +
        "integration potential."
    )
    shuffle_neighborhoods(720)

    box.wait(4)
    gewel.draw.sync([box, teleprompter])

    teleprompter.add_text(
        "That wraps things up for today. I hope you learned something about the difference " +
        "between diversity and integration in communities.\n"
    )

    teleprompter.add_text(
        "Next time, we'll move from the simple made up community we looked at here to " +
        "some real communities, and see how diversity and integration numbers play out" +
        "in the real world.\n"
    )

    teleprompter.add_text(
        "We also have an upcoming video that digs a bit deeper into the math behind how " +
        "we compute the diversity and integration numbers you saw here, and why we chose " +
        "the particular methods we used.\n\n" +
        "Thanks again for watching!"
    )

    gewel.draw.sync([box, teleprompter])
    box.wait(2)

    # Exeunt
    # exeunt(all_drawables_on_stage)

    # box.wait(6)

    # The action of the scene is essentially done at this point, but because
    # we want to compute diversity numbers only at key times, and we did not
    # know all the key times until now, we will go back and construct TVFs for
    # diversity that are sampled at key times, and then we plug those TVS
    # back into the right bars.

    def diversity(
            in_predicate: Callable[[tvx.FloatOrTVF, tvx.FloatOrTVF], tvx.BoolOrTVB]
    ) -> tvx.FloatOrTVF:
        n_iis = [
            # Use once here so we do not re-evaluate this big sum many times
            # when evaluating the expressions for all the p_iis below.
            tvx.once(sum([
                tvx.if_then_else(in_predicate(d.x, d.y), 1.0, 0.0) for d in drawables_ii
            ]))
            for drawables_ii in drawables
        ]
        n = tvx.once(sum(n_iis))
        p_iis = [(n - n_ii) / n for n_ii in n_iis]
        div = sum([p_ii * n_ii for p_ii, n_ii in zip(p_iis, n_iis)]) / n

        result = tvx.if_then_else(n > 0.0, div, 0.0)

        return result

    box_diversity = diversity(lambda x, y: dm.in_box(x, y))

    diversity_a = diversity(lambda x, y: dm.in_neighborhood_a(x, y))
    diversity_b = diversity(lambda x, y: dm.in_neighborhood_b(x, y))
    diversity_c = diversity(lambda x, y: dm.in_neighborhood_c(x, y))

    integration = (diversity_a + diversity_b + diversity_c) / 3.0

    sample_diversity = True

    if sample_diversity:
        if verbose:
            print("Marked {:d} key times.".format(len(key_times)))

        box_diversity = tvx.sample(box_diversity, key_times)
        diversity_a = tvx.sample(diversity_a, key_times)
        diversity_b = tvx.sample(diversity_b, key_times)
        diversity_c = tvx.sample(diversity_c, key_times)

        integration = tvx.sample(integration, key_times)

    fire_color_map_box.position = box_diversity

    fire_color_map_a.position = diversity_a
    fire_color_map_b.position = diversity_b
    fire_color_map_c.position = diversity_c

    fire_color_map_int.position = integration

    dis_bars[BarId.BAR_DIVERSITY_IN_ALL].height = -max_div_box_height * box_diversity

    dis_bars[BarId.BAR_DIVERSITY_IN_A].height = tvx.cut(0.0, n_fade_in_time, -max_div_box_height * diversity_a)
    dis_bars[BarId.BAR_DIVERSITY_IN_B].height = tvx.cut(0.0, n_fade_in_time, -max_div_box_height * diversity_b)
    dis_bars[BarId.BAR_DIVERSITY_IN_C].height = tvx.cut(0.0, n_fade_in_time, -max_div_box_height * diversity_c)

    dis_bars[BarId.BAR_INTEGRATION].height = tvx.cut(
        0.0, n_integration_fade_in_time, -max_div_box_height * integration
    )

    segregation_bar.y = dis_bars[BarId.BAR_INTEGRATION].y + dis_bars[BarId.BAR_INTEGRATION].height
    segregation_bar.height = tvx.cut(
        0.0, n_integration_fade_in_time,
        -max_div_box_height * (box_diversity - integration)
    )

    gewel.draw.sync([box, teleprompter])

    return scene, box.time


def main(args: Optional[List[str]] = None) -> int:
    if args is None:
        args = sys.argv[1:]

    # Note that -h is the hard-coded default for
    # help but we want to use -? instead, so we
    # do a slight hack here.
    parser = argparse.ArgumentParser(add_help=False)
    parser.add_argument(
        "-?", "--help",
        action='help', default=argparse.SUPPRESS,
        help='Show this help message and exit.')

    draft_w, draft_h = gewel.record.RESOLUTION_VGA_16_9
    draft_fps = 24.0

    output_group = parser.add_mutually_exclusive_group(required=True)
    output_group.add_argument("-i", "--interactive", dest="interactive", action='store_true',
                              help="Interactive mode. Display in a window.")
    output_group.add_argument("-o", "--output", type=str,
                              help="Output file path.")

    parser.add_argument('-V', '--verbose', dest='verbose', action='store_true',
                        help='Verbose mode.')
    parser.add_argument('-c', '--clock', dest='clock', action='store_true', default=False,
                        help='Show a clock at the bottom left corner.')
    parser.add_argument('-t', '--teleprompter', dest='teleprompter', action='store_true',
                        default=False,
                        help='Show a teleprompter for voiceover work.')
    parser.add_argument('-p', '--profile', dest='profile', action='store_true',
                        help='Profile the run. Only takes effect for -o, not for -i.')

    parser.add_argument("-s", "--start", type=float, required=False,
                        help="Start rendering this many seconds into the scene. If not used, start at the beginning.")
    parser.add_argument("-e", "--end", type=float, required=False,
                        help="Stop rendering this many seconds into the scene. If not used, render until the end.")

    resolutions = {
        "VGA": gewel.record.RESOLUTION_VGA,
        "VGA-W": gewel.record.RESOLUTION_VGA_16_9,
        "720p": gewel.record.RESOLUTION_720P,
        "1080p": gewel.record.RESOLUTION_1080P,
        "4K": gewel.record.RESOLUTION_4K,
    }
    
    parser.add_argument("-r", "--resolution", choices=["VGA", "VGA-W", "720p", "1080p", "4K"], required=False,
                        help="Output resolution. If present, -h and -w are ignored.")
    parser.add_argument("-w", "--width", type=int, default=gewel.record.RESOLUTION_720P[0],
                        help="Output video width.")
    parser.add_argument("-h", "--height", type=int, default=gewel.record.RESOLUTION_720P[1],
                        help="Output video height.")
    parser.add_argument("-f", "--fps", type=float, default=30.0,
                        help="Output frames per second.")
    parser.add_argument('-scene', '--draft', dest='draft', action='store_true',
                        help="Draft output mode. Equivalent to -w {:} -h {:} -f {:}. " +
                             "Note that -w, -h, and -f are ignored if -scene is passed.".format(
                                 draft_w, draft_h, draft_fps))

    args = parser.parse_args(args)

    if args.verbose:
        print("Verbose mode is on.")
        if args.interactive:
            print("Interactive mode is on.")
        else:
            print("Output: {:}".format(args.output))

    scene_width, scene_height = gewel.record.RESOLUTION_4K

    perf_start = perf_counter()
    scene, scene_duration = build_scene(
        scene_width, scene_height,
        verbose=args.verbose, show_clock=args.clock,
        with_teleprompter=args.teleprompter,
    )
    perf_end = perf_counter()

    if args.verbose:
        print("Prepared a scene of animated duration {:.1f} seconds.".format(scene_duration))
        print("Scene preparation took {:.2f} sec of real time.".format(perf_end - perf_start))

    recording_start = args.start if args.start is not None else 0.0
    recording_end = args.end if args.end is not None else scene_duration
    recording_duration = recording_end - recording_start

    if args.draft:
        if args.verbose:
            print("In verbose mode.")
        width, height = draft_w, draft_h
        fps = draft_fps
    elif args.resolution:
        width, height = resolutions[args.resolution]
        fps = args.fps
    else:
        width, height = args.width, args.height
        fps = args.fps

    if args.verbose:
        print("Scene will be scaled from {:}x{:} to {:}x{:}.".format(
            scene_width, scene_height, width, height
        ))

    scene = gewel.draw.Scene([gewel.record.scale_to_fit(
        scene, scene_width, scene_height, width, height)])

    tp_height = height // 3 if args.teleprompter else 0

    if args.verbose:
        if args.teleprompter:
            print("Adding additional height of {:.0f} for teleprompter.".format(tp_height))

    if args.interactive:
        if args.verbose:
            print("Entering interactive mode.")

        viewer = gewel.player.Player(scene, width, height + tp_height, recording_start, recording_duration)
        viewer.mainloop()
    else:
        recorder = gewel.record.Mp4Recorder(args.output)

        if args.verbose:
            print("Rendering {:} ({:}x{:}; {:.1f} fps)".format(args.output, width, height, fps))
            print("Rendering from time {:.1f} to {:.1f} seconds (duration = {:.1f}).".format(
                recording_start, recording_end, recording_duration))

            if args.profile:
                print("Profiling the rendering process.")

        gewel.draw.reset_total_device_drawing_time()
        recorder.reset_metrics()

        pr = None

        if args.profile:
            pr = cProfile.Profile()
            pr.enable()

        perf_start = perf_counter()
        recorder.record(scene, t0=recording_start, duration=recording_duration,
                        width=width, height=height + tp_height, fps=fps)
        perf_end = perf_counter()

        if args.profile:
            pr.disable()
            s = io.StringIO()
            ps = pstats.Stats(pr, stream=s).sort_stats(pstats.SortKey.TIME)
            ps.print_stats()
            print(s.getvalue())

        if args.verbose:
            frames = recorder.frames_recorded
            total_time = perf_end - perf_start
            device_time = gewel.draw.total_device_drawing_time()
            recording_time = recorder.recording_time

            implied_tvf_time = total_time - (device_time + recording_time)

            print("Renderer recorded {:} frames.".format(frames))

            print("Total rendering time: {:.2f} sec; {:.1f} fps.".format(
                total_time, frames / total_time))
            print("Total device drawing time: {:.2f} sec; {:.1f} fps.".format(
                device_time, frames / device_time))
            print("Renderer recording time: {:.2f} sec; {:.1f} fps.".format(
                recording_time, frames / recording_time))
            print("Implied PyTvf computation time: {:.2f} sec; {:.1f} fps.".format(
                implied_tvf_time, frames / implied_tvf_time
            ))

    return 0


if __name__ == '__main__':
    sys.exit(main())
